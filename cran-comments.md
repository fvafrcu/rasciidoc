Dear CRAN Team,
this is a resubmission of package 'rasciidoc'. I have added the following changes:

* Fixed CRAN notes on Escaped LaTeX specials.

Please upload to CRAN.
Best, Andreas Dominik

# Package rasciidoc 4.1.1

Reporting is done by packager version 1.15.2


## Test environments
- R Under development (unstable) (2023-07-29 r84787)
   Platform: x86_64-pc-linux-gnu
   Running under: Devuan GNU/Linux 4 (chimaera)
   0 errors | 0 warnings | 1 note 
- win-builder (devel)  
  Date: Tue, 15 Aug 2023 23:56:36 +0200
  Your package rasciidoc_4.1.1.tar.gz has been built (if working) and checked for Windows.
  Please check the log files and (if working) the binary package at:
  https://win-builder.r-project.org/DePLurQ617Wi
  The files will be removed after roughly 72 hours.
  Installation time in seconds: 3
  Check time in seconds: 63
  Status: 1 NOTE
  R Under development (unstable) (2023-08-14 r84947 ucrt)
   

## Local test results
- RUnit:
    rasciidoc_unit_test - 14 test functions, 0 errors, 0 failures in 38 checks.
- testthat:
    [ FAIL 0 | WARN 2 | SKIP 0 | PASS 8 ]
- Coverage by covr:
    rasciidoc Coverage: 80.05%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for rasciidoc by 6.
     Exceeding maximum cyclomatic complexity of 10 for hilight_source by 5.
     Exceeding maximum cyclomatic complexity of 10 for get_asciidoc by 3.
- lintr:
    found 235 lints in 851 lines of code (a ratio of 0.2761).
- cleanr:
    found 6 dreadful things about your code.
- codetools::checkUsagePackage:
    found 22 issues.
- devtools::spell_check:
    found 23 unkown words.
