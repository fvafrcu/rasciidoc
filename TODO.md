- pass an option to force spinning / knitting instead of guessing?
  render knit = TRUE -> guess, knit = "knit" -> knit, knit = "spin" -> spin?
- Check on 
  test_adjusting_hooks <- function() {
      if (grepl(paste0("^", dirname(tempdir()), ".*$"), getwd())) {
          warning("skipping test for covr")
      } else {
- improve vignette
- improve test coverage
