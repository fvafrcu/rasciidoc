R=R-devel
PKGNAME=$( sed -n "s/Package: *\([^ ]*\)/\1/p" DESCRIPTION)
PKGVERS=$(sed -n "s/Version: *\([^ ]*\)/\1/p" DESCRIPTION)
${R} --vanilla CMD build .
${R} --vanilla CMD INSTALL  ${PKGNAME}_${PKGVERS}.tar.gz
${R} --vanilla CMD check  ${PKGNAME}_${PKGVERS}.tar.gz
