Dear CRAN Team,
this is a resubmission of package 'rasciidoc'. It was archived on CRAN probably
due to a vignette written in rasciidoc that failed on CRAN/solaris/R-patched.
I have now removed the vignette, hoping that the package will pass on CRAN/solaris/R-patched.
Best, Andreas Dominik

# Package rasciidoc 2.1.3

Reporting is done by packager version 1.3.0.9000

## Test  environments 
- R Under development (unstable) (2020-06-12 r78687)
    Platform: x86_64-pc-linux-gnu (64-bit)
    Running under: Devuan GNU/Linux 3 (beowulf)
    0 errors | 0 warnings | 1 note 
- gitlab.com
  R version 4.0.0 (2020-04-24)
  Platform: x86_64-pc-linux-gnu (64-bit)
  Running under: Ubuntu 20.04 LTS
  Status: 1 ERROR, 1 WARNING, 2 NOTEs
- win-builder (devel)

## Local test results
- RUnit:
    rasciidoc_unit_test - 12 test functions, 0 errors, 0 failures in 32 checks.
- Testthat:
    OK: 1, Failed: 0, Warnings: 1, Skipped: 0
- Coverage by covr:
    rasciidoc Coverage: 65.98%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for hilight_source by 5.
- lintr:
    found 126 lints in 598 lines of code (a ratio of 0.2107).
- cleanr:
    found 17 dreadful things about your code.
- codetools::checkUsagePackage:
    found 22 issues.
- devtools::spell_check:
    found 22 unkown words.
